#include "../inc/candidato.hpp"
#include "../inc/eleitor.hpp"
#define DF "Distrito Federal"
#define BR "Brasil"

Candidato::Candidato () {}


Candidato::~Candidato() {}


std::string Candidato::getNM_UE () {
    return NM_UE;
}

std::string Candidato::getDS_CARGO() {
    return DS_CARGO;
}

std::string Candidato::getNR_CANDIDATO() {
    return NR_CANDIDATO;
}

std::string Candidato::getNM_CANDIDATO() {
    return NM_CANDIDATO;
}

std::string Candidato::getNM_URNA_CANDIDATO() {
    return NM_URNA_CANDIDATO;
}

std::string Candidato::getNR_PARTIDO() {
    return NR_PARTIDO;
}

std::string Candidato::getSG_PARTIDO() {
    return SG_PARTIDO;
}

std::string Candidato::getNM_PARTIDO() {
    return NM_PARTIDO;
}


void Candidato::setDS_CARGO(std::string DS_CARGO) {
    this -> DS_CARGO = DS_CARGO;
}

void Candidato::setNR_CANDIDATO(std::string NR_CANDIDATO) {
    this -> NR_CANDIDATO = NR_CANDIDATO;
}

void Candidato::setNM_CANDIDATO(std::string NM_CANDIDATO) {
    this -> NM_CANDIDATO = NM_CANDIDATO;
}

void Candidato::setNM_URNA_CANDIDATO(std::string NM_URNA_CANDIDATO) {
    this -> NM_URNA_CANDIDATO = NM_URNA_CANDIDATO;
}

void Candidato::setNR_PARTIDO(std::string NR_PARTIDO) {
    this -> NR_PARTIDO = NR_PARTIDO;
} 

void Candidato::setSG_PARTIDO(std::string SG_PARTIDO) {
    this -> SG_PARTIDO = SG_PARTIDO;
}

void Candidato::setNM_PARTIDO(std::string NM_PARTIDO) {
    this -> NM_PARTIDO = NM_PARTIDO;
}

void Candidato::setNM_UE(std::string NM_UE) {
    this -> NM_UE = NM_UE;
}

void Candidato::voteDD() {

    std::cout << "---------------DEPUTADO DISTRITAL---------------" << std::endl;
    std::cout << "Digite o número para deputado distrital(5 dígitos): " << std::endl;
    std::cout << "Votar em branco(Digite 0)" << std::endl;
    std::cout << "Sua opção: ";

    numberCandidate = this -> verificarVoto();

    if(numberCandidate != 0) {
        this -> showCandidate(DF, "DEPUTADO DISTRITAL", numberCandidate);
    }
    else {
        std::cout << "Voto em branco" << std::endl;
        this -> decision("DEPUTADO DISTRITAL", 0);
    }
    system("clear");
}

void Candidato::voteDF() {

    std::cout << "---------------DEPUTADO FEDERAL---------------" << std::endl;
    std::cout << "Digite o número para deputado federal(4 dígitos): " << std::endl;
    std::cout << "Votar em branco(Digite 0)" << std::endl;
    std::cout << "Sua opção: ";

    numberCandidate = this -> verificarVoto();
    

    if(numberCandidate != 0) {
        this -> showCandidate(DF, "DEPUTADO FEDERAL", numberCandidate);
    }   
    else {
        std::cout << "Voto em branco!" << std::endl;
        this -> decision("DEPUTADO FEDERAL", 0);
    }
    system("clear");
}

void Candidato::voteSenador(int vagaSenador) {
    

    if(vagaSenador == 1) {
        std::cout << "---------------SENADOR(PRIMEIRA VAGA)----------------" << std::endl;
        std::cout << "Digite o número para senador(3 dígitos): " << std::endl;
        std::cout << "Votar em branco(Digite 0):" << std::endl;
        std::cout << "Sua opção: ";


        numberCandidate = this -> verificarVoto();

        votoSenador = numberCandidate;

        if(numberCandidate != 0) {
            this -> showCandidate(DF, "SENADOR", numberCandidate, false, 1);
            std::cout << "##########PRIMEIRO SUPLENTE##########" << std::endl;
            this -> showCandidate(DF, "PRIMEIRO SUPLENTE", numberCandidate, false, 1);
            std::cout << "##########SEGUNDO SUPLENTE##########" << std::endl;
            this -> showCandidate(DF, "SEGUNDO SUPLENTE", numberCandidate, true, 1);
        }
        else {
            std::cout << "Voto em branco!" << std::endl;
            this -> decision("SEGUNDO SUPLENTE", 1);
        }
    }

    system("clear");

        if(vagaSenador == 2) {
        std::cout << "---------------SENADOR(SEGUNDA VAGA)---------------" << std::endl;
        std::cout << "Digite o número para senador(3 dígitos): " << std::endl;
        std::cout << "Votar em branco(Digite 0)" << std::endl;
        std::cout << "Sua opção: ";


        numberCandidate = this -> verificarVoto();
        

        if(numberCandidate != 0) {
            if(votoSenador == numberCandidate) {
                numberCandidate = 1111;
            }
            this -> showCandidate(DF, "SENADOR", numberCandidate, false, 2);
            std::cout << "##########PRIMEIRO SUPLENTE##########" << std::endl;
            this -> showCandidate(DF, "PRIMEIRO SUPLENTE", numberCandidate, false, 2);
            std::cout << "##########SEGUNDO SUPLENTE##########" << std::endl;
            this -> showCandidate(DF, "SEGUNDO SUPLENTE", numberCandidate, true, 2);

        }
        else {
            std::cout << "Voto em branco!" << std::endl;
            this -> decision("SEGUNDO SUPLENTE", 2);
        }
    }
    system("clear");
}

void Candidato::voteGovernador() {

    std::cout << "---------------GOVERNADOR---------------" << std::endl;
    std::cout << "Digite o número para Governador(2 dígitos): " << std::endl;
    std::cout << "Caso deseje votar em branco (Digite 0)" << std::endl;
    std::cout << "Sua opção: ";

    numberCandidate = this -> verificarVoto();
    

    if(numberCandidate != 0) {
        this -> showCandidate(DF, "GOVERNADOR", numberCandidate, false, 1);
        std::cout << "##########VICE-GOVERNADOR###########" << std::endl;
        this -> showCandidate(DF, "VICE-GOVERNADOR", numberCandidate, true, 1);
    }
    else {
        std::cout << "Voto em branco!" << std::endl;
        this -> decision("VICE-GOVERNADOR", 1);
    }
    system("clear");
}

void Candidato::votePresidente() {

    std::cout << "---------------PRESIDENTE---------------" << std::endl;
    std::cout << "Digite o número para presidente: " << std::endl;
    std::cout << "Caso deseje votar em branco (Digite 0)" << std::endl;
    std::cout << "Sua opção: ";
    
    numberCandidate = this -> verificarVoto();
    

    if(numberCandidate != 0) {
        this -> showCandidate(BR, "PRESIDENTE", numberCandidate, false, 1);
        std::cout << "##########VICE-PRESIDENTE##########" << std::endl;
        this -> showCandidate(BR, "VICE-PRESIDENTE", numberCandidate, true, 1);
    }
    else {
        std::cout << "Voto em branco!" << std::endl;
        this -> decision("VICE-PRESIDENTE", 1);
    }
    system("clear");
}

void Candidato::showCandidate(std::string ds, std::string cargo, int numberCandidate, bool votar, int posto) {

    std::ifstream readFileBR("data/consulta_cand_2018_BR.csv");


    if(!readFileBR.is_open()) {
        std::cout << "Erro ao abrir o arquivo!" << std::endl;
    }

    if(ds.compare(DF) == 0) {
        std::ifstream readFileDF("data/consulta_cand_2018_DF.csv");
        readFileBR.swap(readFileDF);
  }

  voteNull = true;

  for(int i = 0; i < 1238; ++i) {
    
    getline(readFileBR, NM_UE, ';');
    getline(readFileBR, DS_CARGO, ';');
    getline(readFileBR, NR_CANDIDATO, ';');
    getline(readFileBR, NM_CANDIDATO, ';');
    getline(readFileBR, NM_URNA_CANDIDATO, ';');
    getline(readFileBR, NR_PARTIDO, ';');
    getline(readFileBR, SG_PARTIDO, ';');
    getline(readFileBR, NM_PARTIDO, '\n');

  if(stoi(getNR_CANDIDATO(), nullptr, 10) == numberCandidate && getDS_CARGO().compare(cargo) == 0) {

    std::cout << "UE: " << getNM_UE() << std::endl;
    std::cout << "Cargo: " << getDS_CARGO() << std::endl;
    std::cout << "Número do candidato: " << getNR_CANDIDATO() << std::endl;
    std::cout << "Nome do candidato: " << getNM_CANDIDATO() << std::endl;
    std::cout << "Nome na urna: " << getNM_URNA_CANDIDATO() << std::endl;
    std::cout << "Número do Partido: " << getNR_PARTIDO() << std::endl;
    std::cout << "Sigla do Partido: " << getSG_PARTIDO() << std::endl;
    std::cout << "Nome do Partido: " << getNM_PARTIDO() << std::endl;
    readFileBR.close();

    voteNull = false;
    if(votar) {
        this -> decision(cargo, posto);
    }
    return;
  }

}

if(voteNull) {
    std::cout << "Voto nulo!" << std::endl;
        if(votar) {
            this -> decision(cargo, posto);
        }

        return;
    }
}

void Candidato::showCandidate(std::string ds, std::string cargo, int numberCandidate) {
    

    std::ifstream readFileBR("data/consulta_cand_2018_BR.csv");

    if(!readFileBR.is_open()) {
    std::cout << "Erro ao abrir o arquivo!" << std::endl;
  }

  if(ds.compare(DF) == 0) {
    std::ifstream readFileDF("data/consulta_cand_2018_DF.csv");
    readFileBR.swap(readFileDF);
  }

  voteNull = true;

  for(int i = 0; i < 1238; ++i) {

    getline(readFileBR, NM_UE, ';');
    getline(readFileBR, DS_CARGO, ';');
    getline(readFileBR, NR_CANDIDATO, ';');
    getline(readFileBR, NM_CANDIDATO, ';');
    getline(readFileBR, NM_URNA_CANDIDATO, ';');
    getline(readFileBR, NR_PARTIDO, ';');
    getline(readFileBR, SG_PARTIDO, ';');
    getline(readFileBR, NM_PARTIDO, '\n');

  if(stoi(getNR_CANDIDATO(), nullptr, 10) == numberCandidate && getDS_CARGO().compare(cargo) == 0) {
    
    std::cout << "UE: " << getNM_UE() << std::endl;
    std::cout << "Cargo: " << getDS_CARGO() << std::endl;
    std::cout << "Número do candidato: " << getNR_CANDIDATO() << std::endl;
    std::cout << "Nome do candidato: " << getNM_CANDIDATO() << std::endl;
    std::cout << "Nome na urna: " << getNM_URNA_CANDIDATO() << std::endl;
    std::cout << "Número do Partido: " << getNR_PARTIDO() << std::endl;
    std::cout << "Sigla do Partido: " << getSG_PARTIDO() << std::endl;
    std::cout << "Nome do Partido: " << getNM_PARTIDO() << std::endl;

    readFileBR.close();

    voteNull = false;
    this -> decision(cargo, 0);

    return;
    }
}

if(voteNull) {
    std::cout << "Voto nulo!" << std::endl;
    this -> decision(cargo, 0);

    return;
    }
}

int Candidato::verificarVoto() {

    while(1) {
        std::cin >> voteUrna;
        if(voteUrna.compare("0") == 0) {
            return 0;
        }
        else {
            try {
                return stoi(voteUrna, nullptr, 10);
            }

            catch(std::exception &a) {
                std::cout << "Por favor, digite corretamente!" << std::endl;
            }
        }
    }
}


void Candidato::decision(std::string cargo, int posto) {
    while(1) {
        std::string decisionPessoa;
        std::cout << "Digite 1 para confirmar seu voto" << std::endl;
        std::cout << "Digite 2 para corrigir seu voto" << std::endl;
        std::cout << "Sua opção(1 ou 2): ";
        std::cin >> decisionPessoa;
        system("clear");

        if(decisionPessoa.compare("2") == 0) {

            if(cargo.compare("DEPUTADO DISTRITAL") == 0) {
                this -> voteDD();
                break;
            }

            if(cargo.compare("DEPUTADO FEDERAL") == 0) {
                this -> voteDF();
                break;
            }

            if(cargo.compare("PRIMEIRO SUPLENTE") == 0 && posto == 1) {
                this -> voteSenador(posto);
                break;
            }

            if(cargo.compare("SEGUNDO SUPLENTE") == 0 && posto == 2) {
                this -> voteSenador(posto);
                break;
            }

            if(cargo.compare("VICE-GOVERNADOR") == 0) {
                this -> voteGovernador();
                break;
            }

            if(cargo.compare("VICE-PRESIDENTE") == 0) {
                this -> votePresidente();
                break;
            }
        }
        else if(decisionPessoa.compare("1") == 0) {
            std::cout << "Voto confirmado com sucesso!" << std::endl;
            break;
        }
        else {
            std::cout << "Digite de novo!" << std::endl;
        }
    }
}