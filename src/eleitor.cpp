#include "../inc/eleitor.hpp"
#include "../inc/pessoa.hpp"
#include "../inc/candidato.hpp"

Eleitor::Eleitor () {

	setnomeCompleto("");
	dataNascimento = "";
	cpf = "";
	tituloEleitor = 0;
}

Eleitor::~Eleitor(){}


std::string Eleitor::getnomeCompleto() {
	return nomeCompleto;
}

std::string Eleitor::getdataNascimento() {
    return dataNascimento;
}


std::string Eleitor::getcpf() {
	return cpf;
}


long int Eleitor::gettituloEleitor() {
	return tituloEleitor;
}

void Eleitor::setnomeCompleto(std::string nomeCompleto) {
	this -> nomeCompleto = nomeCompleto;
}

void Eleitor::setdataNascimento(std::string dataNascimento) {
    this -> dataNascimento = dataNascimento;
}


void Eleitor::setcpf(std::string cpf) {
	this -> cpf = cpf;
}


void Eleitor::settituloEleitor(long int tituloEleitor) {
	this -> tituloEleitor = tituloEleitor;
}

void Eleitores(Eleitor& eleitor) {

	std::string nomeCompleto, dataNascimento, cpf;
	long int tituloEleitor;
	std::cout << "Informe o seu nome(Apenas letras): ";
	std::cin.ignore();
	getline(std::cin, nomeCompleto);
	eleitor.setnomeCompleto(nomeCompleto);

	std::cout << "Informe sua data de nascimento(DD/MM/AA): ";
	getline(std::cin, dataNascimento);
	eleitor.setdataNascimento(dataNascimento);

	std::cout << "Informe o CPF(Apenas números): ";
	getline(std::cin, cpf);
	eleitor.setcpf(cpf);

	std::cout << "Informe seu título de eleitor(Apenas números): ";
	std::cin >> tituloEleitor;
	std::cin.ignore();
	eleitor.settituloEleitor(tituloEleitor);

}

void imprimeDados(Eleitor& eleitor) {
	std::cout << "Nome do eleitor: " << eleitor.getnomeCompleto() << std::endl;
	std::cout << "Data de nascimento do eleitor: " << eleitor.getdataNascimento() << std::endl;
	std::cout << "CPF do eleitor: " << eleitor.getcpf() << std::endl;
	std::cout << "Título do eleitor: " << eleitor.gettituloEleitor() << std::endl;
}
